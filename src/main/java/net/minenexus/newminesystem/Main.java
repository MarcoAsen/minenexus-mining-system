package net.minenexus.newminesystem;

import net.minenexus.newminesystem.chunk.ChunkLoad;
import net.minenexus.newminesystem.chunk.NewOreRemover;
import net.minenexus.newminesystem.mining.Mining;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main extends JavaPlugin {



    public FixedMetadataValue spawnedBlock = new FixedMetadataValue(this, "metadata");

    private static Main main;
    public static Main getInstance() {
        return main;
    }
    private ArrayList<Material> ores = new ArrayList<>();
    public boolean DEBUG = false;

    @Override
    public void onEnable() {
        Bukkit.getLogger().info("Attempting to connect to authentication server...");
        Bukkit.getPluginManager().registerEvents(new ChunkLoad(), this);
        Bukkit.getPluginManager().registerEvents(new Mining(), this);
        Bukkit.getPluginManager().registerEvents(new NewOreRemover(), this);
        saveDefaultConfig();
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                for (Player player : Bukkit.getOnlinePlayers()) {
                if (Main.getInstance().isInCorrectWorld(player)) {
                    List<Block> nearbyBlocks = getNearbyBlocks(player.getLocation(), 35);
                    for (Block block : nearbyBlocks) {
                        if (isOre(block.getType())) {
                            Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
                                @Override
                                public void run() {
                                    if (!block.hasMetadata("generated")) {
                                        block.setType(Material.STONE);
                                    }
                                }
                            }, 20L);

                        }
                    }
                }

                }
            }
        }, 40L, 40L);
        super.onEnable();
    }

    @Override
    public void onDisable() {
        saveConfig();
        super.onDisable();
    }

    @Override
    public void onLoad() {
        main = this;
        super.onLoad();
    }

    public boolean isInCorrectWorld(Player player) {
        return player.getWorld().getName().equals(getConfig().getString("mundo"));
    }

    private static List<Block> getNearbyBlocks(Location location, int radius) {
        List<Block> blocks = new ArrayList<Block>();
        for (int x = location.getBlockX() - radius; x <= location.getBlockX() + radius; x++) {
            for (int y = location.getBlockY() - radius; y <= location.getBlockY() + radius; y++) {
                for (int z = location.getBlockZ() - radius; z <= location.getBlockZ() + radius; z++) {
                    blocks.add(location.getWorld().getBlockAt(x, y, z));
                }
            }
        }
        return blocks;
    }

    private boolean isOre(Material material) {
        if (ores.isEmpty()) {
            ores.add(Material.COAL_ORE);
            ores.add(Material.IRON_ORE);
            ores.add(Material.GOLD_ORE);
            ores.add(Material.REDSTONE_ORE);
            ores.add(Material.DIAMOND_ORE);
            ores.add(Material.EMERALD_ORE);
            ores.add(Material.LAPIS_ORE);
            ores.add(Material.GLOWING_REDSTONE_ORE);
        }
        return ores.contains(material);
    }



}
