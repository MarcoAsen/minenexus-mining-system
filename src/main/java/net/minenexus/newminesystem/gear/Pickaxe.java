package net.minenexus.newminesystem.gear;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.HashSet;

public class Pickaxe {

    private static HashSet<Material> wooden = new HashSet<>();
    private static HashSet<Material> stone = new HashSet<>();
    private static HashSet<Material> iron = new HashSet<>();

    public static boolean isLowTierWooden(ItemStack itemStack) {
        if (itemStack == null) return false;
        if (wooden.isEmpty()) {
            wooden.add(Material.WOOD_PICKAXE);
            wooden.add(Material.STONE_PICKAXE);
            wooden.add(Material.IRON_PICKAXE);
            wooden.add(Material.GOLD_PICKAXE);
            wooden.add(Material.DIAMOND_PICKAXE);
        }

        return wooden.contains(itemStack.getType());
    }

    public static boolean isLowTierStone(ItemStack itemStack) {
        if (itemStack == null) return false;
        if (stone.isEmpty()) {
            stone.add(Material.STONE_PICKAXE);
            stone.add(Material.IRON_PICKAXE);
            stone.add(Material.GOLD_PICKAXE);
            stone.add(Material.DIAMOND_PICKAXE);
        }

        return stone.contains(itemStack.getType());
    }

    public static boolean isLowTierIron(ItemStack itemStack) {
        if (itemStack == null) return false;
        if (iron.isEmpty()) {
            iron.add(Material.IRON_PICKAXE);
            iron.add(Material.GOLD_PICKAXE);
            iron.add(Material.DIAMOND_PICKAXE);
        }

        return iron.contains(itemStack.getType());
    }

}
