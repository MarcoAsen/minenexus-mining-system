package net.minenexus.newminesystem.mining;

import net.minenexus.newminesystem.Main;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import net.minenexus.newminesystem.utils.particles.ParticleEffect;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SpawnOre {

    private static List<Block> getNearbyBlocks(Location location) {
        List<Block> blocks = new ArrayList<Block>();
        for (int x = location.getBlockX() - 1; x <= location.getBlockX() + 1; x++) {
            for (int y = location.getBlockY() - 1; y <= location.getBlockY() + 1; y++) {
                for (int z = location.getBlockZ() - 1; z <= location.getBlockZ() + 1; z++) {
                    blocks.add(location.getWorld().getBlockAt(x, y, z));
                }
            }
        }
        return blocks;
    }

    public static boolean spawnCoal(Player player, Block block) {
        boolean spawn = new Random().nextInt(33) == 1;
        if (spawn) {
            int amount = 12;
            int spawned = 0;
            for (Block blocks : getNearbyBlocks(block.getLocation())) {
                if (spawned != amount) {
                    if (block.getType() == Material.STONE) {
                        blocks.setType(Material.COAL_ORE);
                        addMetadata(blocks);
                        ParticleEffect.VILLAGER_HAPPY.display(2F, 2F, 2F, 0F, 20, blocks.getLocation(), player);
                        spawned++;
                    }
                }

            }
            block.setType(Material.COAL_ORE);
            addMetadata(block);
            ParticleEffect.VILLAGER_HAPPY.display(2F, 2F, 2F, 0F, 20, block.getLocation(), player);
            player.sendMessage(ChatColor.DARK_AQUA + "Você achou uma veia de carvão mineral!");
            Conditions.setDelay(player, (long) (20 * 1000));

        }
        return spawn;
    }

    public static boolean spawnIron(Player player, Block block) {
        boolean spawn = new Random().nextInt(66) == 1;
        if (spawn) {
            int amount = 10;
            int spawned = 0;
            for (Block blocks : getNearbyBlocks(block.getLocation())) {
                if (spawned != amount) {
                    if (block.getType() == Material.STONE) {
                        blocks.setType(Material.IRON_ORE);
                        addMetadata(blocks);
                        ParticleEffect.VILLAGER_HAPPY.display(2F, 2F, 2F, 0F, 20, blocks.getLocation(), player);
                        spawned++;
                    }
                }

            }
            block.setType(Material.IRON_ORE);
            addMetadata(block);
            ParticleEffect.VILLAGER_HAPPY.display(2F, 2F, 2F, 0F, 20, block.getLocation(), player);
            player.sendMessage(ChatColor.DARK_AQUA + "Você achou uma veia de minério de ferro!");
            Conditions.setDelay(player, (long) (20 * 1000));

        }
        return spawn;
    }

    public static boolean spawnLazuli(Player player, Block block) {
        boolean spawn = new Random().nextInt(143) == 1;
        if (spawn) {
            int amount = 10;
            int spawned = 0;
            for (Block blocks : getNearbyBlocks(block.getLocation())) {
                if (spawned != amount) {
                    if (block.getType() == Material.STONE) {
                        blocks.setType(Material.LAPIS_ORE);
                        addMetadata(blocks);
                        ParticleEffect.VILLAGER_HAPPY.display(2F, 2F, 2F, 0F, 20, blocks.getLocation(), player);
                        spawned++;
                    }
                }

            }
            block.setType(Material.LAPIS_ORE);
            addMetadata(block);
            ParticleEffect.VILLAGER_HAPPY.display(2F, 2F, 2F, 0F, 20, block.getLocation(), player);
            player.sendMessage(ChatColor.DARK_AQUA + "Você achou uma veia de minério de lapislazuli!");
            Conditions.setDelay(player, (long) (20 * 1000));

        }
        return spawn;
    }

    public static boolean spawnGold(Player player, Block block) {
        boolean spawn = new Random().nextInt(100) == 1;
        if (spawn) {
            int amount = 6;
            int spawned = 0;
            for (Block blocks : getNearbyBlocks(block.getLocation())) {
                if (spawned != amount) {
                    if (block.getType() == Material.STONE) {
                        blocks.setType(Material.GOLD_ORE);
                        addMetadata(blocks);
                        ParticleEffect.VILLAGER_HAPPY.display(2F, 2F, 2F, 0F, 20, blocks.getLocation(), player);
                        spawned++;
                    }
                }

            }
            block.setType(Material.GOLD_ORE);
            addMetadata(block);
            ParticleEffect.VILLAGER_HAPPY.display(2F, 2F, 2F, 0F, 20, block.getLocation(), player);
            player.sendMessage(ChatColor.DARK_AQUA + "Você achou uma veia de minério de ouro!");
            Conditions.setDelay(player, (long) (20 * 1000));

        }
        return spawn;
    }

    public static boolean spawnDiamonds(Player player, Block block) {
        boolean spawn = new Random().nextInt(333) == 1;
        if (spawn) {
            int amount = 5;
            int spawned = 0;
            for (Block blocks : getNearbyBlocks(block.getLocation())) {
                if (spawned != amount) {
                    if (block.getType() == Material.STONE) {
                        blocks.setType(Material.DIAMOND_ORE);
                        addMetadata(blocks);
                        ParticleEffect.VILLAGER_HAPPY.display(2F, 2F, 2F, 0F, 20, blocks.getLocation(), player);
                        spawned++;
                    }
                }

            }
            block.setType(Material.DIAMOND_ORE);
            addMetadata(block);
            ParticleEffect.VILLAGER_HAPPY.display(2F, 2F, 2F, 0F, 20, block.getLocation(), player);
            player.sendMessage(ChatColor.DARK_AQUA + "Você achou uma veia de diamantes!");
            Conditions.setDelay(player, (long) (30 * 1000));

        }
        return spawn;
    }

    public static boolean spawnRedstone(Player player, Block block) {
        boolean spawn = new Random().nextInt(143) == 1;
        if (spawn) {
            int amount = 7;
            int spawned = 0;
            for (Block blocks : getNearbyBlocks(block.getLocation())) {
                if (spawned != amount) {
                    if (block.getType() == Material.STONE) {
                        blocks.setType(Material.REDSTONE_ORE);
                        addMetadata(blocks);
                        ParticleEffect.VILLAGER_HAPPY.display(2F, 2F, 2F, 0F, 20, blocks.getLocation(), player);
                        spawned++;
                    }
                }

            }
            block.setType(Material.REDSTONE_ORE);
            addMetadata(block);
            ParticleEffect.VILLAGER_HAPPY.display(2F, 2F, 2F, 0F, 20, block.getLocation(), player);
            player.sendMessage(ChatColor.DARK_AQUA + "Você achou uma veia de redstone!");
            Conditions.setDelay(player, (long) (20 * 1000));
        }
        return spawn;
    }

    public static boolean spawnEmerald(Player player, Block block) {
        boolean spawn = new Random().nextInt(1000 * 3) == 1;
        if (spawn) {
            int amount = 3;
            int spawned = 0;
            for (Block blocks : getNearbyBlocks(block.getLocation())) {
                if (spawned != amount) {
                    if (block.getType() == Material.STONE) {
                        blocks.setType(Material.EMERALD_ORE);
                        addMetadata(blocks);
                        ParticleEffect.VILLAGER_HAPPY.display(2F, 2F, 2F, 0F, 20, blocks.getLocation(), player);
                        spawned++;
                    }
                }

            }
            block.setType(Material.EMERALD_ORE);
            addMetadata(block);
            ParticleEffect.VILLAGER_HAPPY.display(2F, 2F, 2F, 0F, 20, block.getLocation(), player);
            player.sendMessage(ChatColor.DARK_AQUA + "Você achou uma veia de esmeralda!");
            Conditions.setDelay(player, (long) (30 * 1000));

        }
        return spawn;
    }

    private static void addMetadata(Block block) {
        if (!block.hasMetadata("generated")) {
            block.setMetadata("generated", Main.getInstance().spawnedBlock);
        }
    }

}
