package net.minenexus.newminesystem.mining;

import net.minenexus.newminesystem.Main;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class Mining implements Listener {

    @EventHandler
    public void playerBreakEvent(BlockBreakEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();
        boolean spawned = false;
        if (player.getWorld().getName().equalsIgnoreCase(Main.getInstance().getConfig().getString("mundo"))) {
            if (block.getType() == Material.STONE) {
                boolean canSpawn = true;
                if (Conditions.isDelayed(player)) {
                    canSpawn = false;
                }
                if (Conditions.canSpawnCoal(player) && canSpawn) {
                    spawned = SpawnOre.spawnCoal(player, block);
                }
                if (!spawned) {
                    if (Conditions.canSpawnIron(player) && canSpawn) {
                        spawned = SpawnOre.spawnIron(player, block);
                    }
                }
                if (!spawned) {
                    if (Conditions.canSpawnLazuli(player) && canSpawn) {
                        spawned = SpawnOre.spawnLazuli(player, block);
                    }
                }
                if (!spawned) {
                    if (Conditions.canSpawnGold(player) && canSpawn) {
                        spawned = SpawnOre.spawnGold(player, block);
                    }
                }
                if (!spawned) {
                    if (Conditions.canSpawnDiamond(player) && canSpawn) {
                        spawned = SpawnOre.spawnDiamonds(player, block);
                    }
                }
                if (!spawned) {
                    if (Conditions.canSpawnRedstone(player) && canSpawn) {
                        spawned = SpawnOre.spawnRedstone(player, block);
                    }
                }
                if (!spawned) {
                    if (Conditions.canSpawnEmerald(player) && canSpawn) {
                        spawned = SpawnOre.spawnEmerald(player, block);
                    }
                }
                event.setCancelled(spawned);
            }
        }
    }

}
