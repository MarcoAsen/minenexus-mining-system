package net.minenexus.newminesystem.mining;

import net.minenexus.newminesystem.gear.Pickaxe;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;
import net.minenexus.newminesystem.utils.GearUtils;

import java.util.HashMap;

public class Conditions {

    public static HashMap<Player, Long> delay = new HashMap<>();

    public static void setDelay(Player player, Long time) {
        if (delay.containsKey(player)) {
            delay.remove(player);
        }
        delay.put(player, System.currentTimeMillis() + time);
    }

    public static boolean isDelayed(Player player) {
        if (delay.containsKey(player)) {
            if (delay.get(player) < System.currentTimeMillis()) {
                delay.remove(player);
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean canSpawnCoal(Player player) {
        if (GearUtils.isPickaxe(player.getInventory().getItemInMainHand()) && player.getLocation().getY() < 132) {
            return true;
        }
        return false;
    }

    public static boolean canSpawnIron(Player player) {
        if (GearUtils.isPickaxe(player.getInventory().getItemInMainHand()) && Pickaxe.isLowTierStone(player.getInventory().getItemInMainHand()) && player.getLocation().getY() < 68) {
            return true;
        }
        return false;
    }

    public static boolean canSpawnLazuli(Player player) {
        if (GearUtils.isPickaxe(player.getInventory().getItemInMainHand()) && Pickaxe.isLowTierStone(player.getInventory().getItemInMainHand()) && player.getLocation().getY() < 34) {
            return true;
        }
        return false;
    }

    public static boolean canSpawnGold(Player player) {
        if (GearUtils.isPickaxe(player.getInventory().getItemInMainHand()) && Pickaxe.isLowTierIron(player.getInventory().getItemInMainHand()) && player.getLocation().getY() < 34) {
            return true;
        }
        return false;
    }

    public static boolean canSpawnDiamond(Player player) {
        if (GearUtils.isPickaxe(player.getInventory().getItemInMainHand()) && Pickaxe.isLowTierIron(player.getInventory().getItemInMainHand()) && player.getLocation().getY() < 16) {
            return true;
        }
        return false;
    }

    public static boolean canSpawnRedstone(Player player) {
        return canSpawnDiamond(player);
    }

    public static boolean canSpawnEmerald(Player player) {
        if (GearUtils.isPickaxe(player.getInventory().getItemInMainHand()) && Pickaxe.isLowTierIron(player.getInventory().getItemInMainHand()) && player.getLocation().getBlock().getBiome().equals(Biome.EXTREME_HILLS) && player.getLocation().getY() < 33) {
            return true;
        }
        return false;
    }

}
