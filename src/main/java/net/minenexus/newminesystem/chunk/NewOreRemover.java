package net.minenexus.newminesystem.chunk;

import net.minenexus.newminesystem.Main;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.metadata.MetadataValue;

import java.util.ArrayList;
import java.util.List;

public class NewOreRemover implements Listener {

    private ArrayList<Material> ores = new ArrayList<>();

    private static List<Block> getNearbyBlocks(Location location) {
        List<Block> blocks = new ArrayList<Block>();
        for (int x = location.getBlockX() - 5; x <= location.getBlockX() + 5; x++) {
            for (int y = location.getBlockY() - 5; y <= location.getBlockY() + 5; y++) {
                for (int z = location.getBlockZ() - 5; z <= location.getBlockZ() + 5; z++) {
                    blocks.add(location.getWorld().getBlockAt(x, y, z));
                }
            }
        }
        return blocks;
    }


    private boolean isOre(Material material) {
        if (ores.isEmpty()) {
            ores.add(Material.COAL_ORE);
            ores.add(Material.IRON_ORE);
            ores.add(Material.GOLD_ORE);
            ores.add(Material.REDSTONE_ORE);
            ores.add(Material.DIAMOND_ORE);
            ores.add(Material.EMERALD_ORE);
            ores.add(Material.LAPIS_ORE);
            ores.add(Material.GLOWING_REDSTONE_ORE);
        }
        return ores.contains(material);
    }

    @EventHandler
    public void playerBreakBlock(BlockBreakEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();
        if (Main.getInstance().isInCorrectWorld(player)) {
            if (isOre(block.getType())) {
                if (block.hasMetadata("generated")) {
                    return;
                }
                List<Block> nearbyBlocks = getNearbyBlocks(block.getLocation());
                for (Block block1 : nearbyBlocks) {
                    if (isOre(block1.getType())) {
                        if (!block.hasMetadata("generated")) {
                            block1.setType(Material.STONE);
                        }
                    }
                }
                block.setType(Material.STONE);
                event.setCancelled(true);

            }
        }
    }

}
