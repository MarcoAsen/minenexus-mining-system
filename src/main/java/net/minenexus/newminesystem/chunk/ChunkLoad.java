package net.minenexus.newminesystem.chunk;

import net.minenexus.newminesystem.Main;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkPopulateEvent;

public class ChunkLoad implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onChunkLoad(ChunkPopulateEvent event) {
        Chunk chunk = event.getChunk();
        if (chunk.getWorld().getName().equalsIgnoreCase(Main.getInstance().getConfig().getString("mundo"))) {
            replaceOreInChunk(chunk, Material.COAL_ORE, Material.STONE);
            replaceOreInChunk(chunk, Material.IRON_ORE, Material.STONE);
            replaceOreInChunk(chunk, Material.GOLD_ORE, Material.STONE);
            replaceOreInChunk(chunk, Material.REDSTONE_ORE, Material.STONE);
            replaceOreInChunk(chunk, Material.DIAMOND_ORE, Material.STONE);
            replaceOreInChunk(chunk, Material.EMERALD_ORE, Material.STONE);
            replaceOreInChunk(chunk, Material.LAPIS_ORE, Material.STONE);
            replaceOreInChunk(chunk, Material.GLOWING_REDSTONE_ORE, Material.STONE);
        }
    }

    public static void replaceOreInChunk(Chunk chunk, Material replaced, Material replacer) {
        for (int x = 0; x < 16; x++) {
            for (int z = 0; z < 16; z++) {
                for (int y = 0; y < 128; y++) {
                    Block block = chunk.getBlock(x, y, z);
                    if (block.getType().equals(replaced)) {
                        block.setType(replacer);
                        if (Main.getInstance().DEBUG) {
                            System.out.println("REPLACED ORE AT [" + x + ", " + y + ", " + z + "]");
                        }
                    }
                }
            }
        }
    }

}
