package net.minenexus.newminesystem.nbt;

import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Method;
import java.util.Set;
import java.util.Stack;

public class NBTReflection {

    private static final String version = org.bukkit.Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];

    private static Class getCraftItemStack() {
        try {
            return Class.forName("org.bukkit.craftbukkit." + version + ".inventory.CraftItemStack");
        } catch (Exception ex) {
            System.out.println("Error in ItemNBTAPI! (Outdated plugin?)");
            ex.printStackTrace();
        }
        return null;
    }

    private static Class getNBTBase() {
        try {
            return Class.forName("net.minecraft.server." + version + ".NBTBase");
        } catch (Exception ex) {
            System.out.println("Error in ItemNBTAPI! (Outdated plugin?)");
            ex.printStackTrace();
        }
        return null;
    }

    private static Class getNBTTagCompound() {
        try {
            return Class.forName("net.minecraft.server." + version + ".NBTTagCompound");
        } catch (Exception ex) {
            System.out.println("Error in ItemNBTAPI! (Outdated plugin?)");
            ex.printStackTrace();
        }
        return null;
    }

    private static Object getNewNBTTag() {
        String version = org.bukkit.Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
        try {
            Class c = Class.forName("net.minecraft.server." + version + ".NBTTagCompound");
            return c.newInstance();
        } catch (Exception ex) {
            System.out.println("Error in ItemNBTAPI! (Outdated plugin?)");
            ex.printStackTrace();
        }
        return null;
    }

    private static Object setNBTTag(Object NBTTag, Object NMSItem) {
        try {
            Method method = NMSItem.getClass().getMethod("setTag", new Class[]{NBTTag.getClass()});
            method.invoke(NMSItem, new Object[]{NBTTag});
            return NMSItem;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Object getNMSItemStack(ItemStack item) {
        Class cis = getCraftItemStack();
        try {
            Method method = cis.getMethod("asNMSCopy", new Class[]{ItemStack.class});
            return method.invoke(cis, new Object[]{item});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static ItemStack getBukkitItemStack(Object item) {
        Class cis = getCraftItemStack();
        try {
            Method method = cis.getMethod("asCraftMirror", new Class[]{item.getClass()});
            Object answer = method.invoke(cis, new Object[]{item});
            return (ItemStack) answer;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object getRootNBTTagCompound(Object nmsitem) {
        Class c = nmsitem.getClass();
        try {
            Method method = c.getMethod("getTag", new Class[0]);
            return method.invoke(nmsitem, new Object[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object getSubNBTTagCompound(Object compound, String name) {
        Class c = compound.getClass();
        try {
            Method method = c.getMethod("getCompound", new Class[]{String.class});
            return method.invoke(compound, new Object[]{name});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ItemStack addNBTTagCompound(ItemStack item, NBTCompound comp, String name) {
        if (name == null) {
            return remove(item, comp, name);
        }
        Object nmsitem = getNMSItemStack(item);
        if (nmsitem == null) {
            System.out.println("Got null! (Outdated Plugin?)");
            return null;
        }
        Object nbttag = getRootNBTTagCompound(nmsitem);
        if (nbttag == null) {
            nbttag = getNewNBTTag();
        }
        if (!valideCompound(item, comp).booleanValue()) {
            return item;
        }
        Object workingtag = gettoCompount(nbttag, comp);
        try {
            Method method = workingtag.getClass().getMethod("set", new Class[]{String.class, getNBTBase()});
            method.invoke(workingtag, new Object[]{name, getNBTTagCompound().newInstance()});
            nmsitem = setNBTTag(nbttag, nmsitem);
            return getBukkitItemStack(nmsitem);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return item;
    }

    public static Boolean valideCompound(ItemStack item, NBTCompound comp) {
        Object root = getRootNBTTagCompound(getNMSItemStack(item));
        if (root == null) {
            root = getNewNBTTag();
        }
        if (gettoCompount(root, comp) != null) {
            return Boolean.valueOf(true);
        }
        return Boolean.valueOf(false);
    }

    private static Object gettoCompount(Object nbttag, NBTCompound comp) {
        Stack<String> structure = new Stack();
        while (comp.getParent() != null) {
            structure.add(comp.getName());
            comp = comp.getParent();
        }
        while (!structure.isEmpty()) {
            nbttag = getSubNBTTagCompound(nbttag, (String) structure.pop());
            if (nbttag == null) {
                return null;
            }
        }
        return nbttag;
    }

    public static ItemStack setString(ItemStack item, NBTCompound comp, String key, String text) {
        if (text == null) {
            return remove(item, comp, key);
        }
        Object nmsitem = getNMSItemStack(item);
        if (nmsitem == null) {
            System.out.println("Got null! (Outdated Plugin?)");
            return null;
        }
        Object rootnbttag = getRootNBTTagCompound(nmsitem);
        if (rootnbttag == null) {
            rootnbttag = getNewNBTTag();
        }
        if (!valideCompound(item, comp).booleanValue()) {
            return item;
        }
        Object workingtag = gettoCompount(rootnbttag, comp);
        try {
            Method method = workingtag.getClass().getMethod("setString", new Class[]{String.class, String.class});
            method.invoke(workingtag, new Object[]{key, text});
            nmsitem = setNBTTag(rootnbttag, nmsitem);
            return getBukkitItemStack(nmsitem);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return item;
    }

    public static String getString(ItemStack item, NBTCompound comp, String key) {
        Object nmsitem = getNMSItemStack(item);
        if (nmsitem == null) {
            System.out.println("Got null! (Outdated Plugin?)");
            return null;
        }
        Object rootnbttag = getRootNBTTagCompound(nmsitem);
        if (rootnbttag == null) {
            rootnbttag = getNewNBTTag();
        }
        if (!valideCompound(item, comp).booleanValue()) {
            return null;
        }
        Object workingtag = gettoCompount(rootnbttag, comp);
        try {
            Method method = workingtag.getClass().getMethod("getString", new Class[]{String.class});
            return (String) method.invoke(workingtag, new Object[]{key});
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static ItemStack setInt(ItemStack item, NBTCompound comp, String key, Integer i) {
        if (i == null) {
            return remove(item, comp, key);
        }
        Object nmsitem = getNMSItemStack(item);
        if (nmsitem == null) {
            System.out.println("Got null! (Outdated Plugin?)");
            return null;
        }
        Object rootnbttag = getRootNBTTagCompound(nmsitem);
        if (rootnbttag == null) {
            rootnbttag = getNewNBTTag();
        }
        if (!valideCompound(item, comp).booleanValue()) {
            return item;
        }
        Object workingtag = gettoCompount(rootnbttag, comp);
        try {
            Method method = workingtag.getClass().getMethod("setInt", new Class[]{String.class, Integer.TYPE});
            method.invoke(workingtag, new Object[]{key, i});
            nmsitem = setNBTTag(rootnbttag, nmsitem);
            return getBukkitItemStack(nmsitem);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return item;
    }

    public static Integer getInt(ItemStack item, NBTCompound comp, String key) {
        Object nmsitem = getNMSItemStack(item);
        if (nmsitem == null) {
            System.out.println("Got null! (Outdated Plugin?)");
            return null;
        }
        Object rootnbttag = getRootNBTTagCompound(nmsitem);
        if (rootnbttag == null) {
            rootnbttag = getNewNBTTag();
        }
        if (!valideCompound(item, comp).booleanValue()) {
            return null;
        }
        Object workingtag = gettoCompount(rootnbttag, comp);
        try {
            Method method = workingtag.getClass().getMethod("getInt", new Class[]{String.class});
            return (Integer) method.invoke(workingtag, new Object[]{key});
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static ItemStack setDouble(ItemStack item, NBTCompound comp, String key, Double d) {
        if (d == null) {
            return remove(item, comp, key);
        }
        Object nmsitem = getNMSItemStack(item);
        if (nmsitem == null) {
            System.out.println("Got null! (Outdated Plugin?)");
            return null;
        }
        Object rootnbttag = getRootNBTTagCompound(nmsitem);
        if (rootnbttag == null) {
            rootnbttag = getNewNBTTag();
        }
        if (!valideCompound(item, comp).booleanValue()) {
            return item;
        }
        Object workingtag = gettoCompount(rootnbttag, comp);
        try {
            Method method = workingtag.getClass().getMethod("setDouble", new Class[]{String.class, Double.TYPE});
            method.invoke(workingtag, new Object[]{key, d});
            nmsitem = setNBTTag(rootnbttag, nmsitem);
            return getBukkitItemStack(nmsitem);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return item;
    }

    public static Double getDouble(ItemStack item, NBTCompound comp, String key) {
        Object nmsitem = getNMSItemStack(item);
        if (nmsitem == null) {
            System.out.println("Got null! (Outdated Plugin?)");
            return null;
        }
        Object rootnbttag = getRootNBTTagCompound(nmsitem);
        if (rootnbttag == null) {
            rootnbttag = getNewNBTTag();
        }
        if (!valideCompound(item, comp).booleanValue()) {
            return null;
        }
        Object workingtag = gettoCompount(rootnbttag, comp);
        try {
            Method method = workingtag.getClass().getMethod("getDouble", new Class[]{String.class});
            return (Double) method.invoke(workingtag, new Object[]{key});
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static ItemStack setBoolean(ItemStack item, NBTCompound comp, String key, Boolean d) {
        if (d == null) {
            return remove(item, comp, key);
        }
        Object nmsitem = getNMSItemStack(item);
        if (nmsitem == null) {
            System.out.println("Got null! (Outdated Plugin?)");
            return null;
        }
        Object rootnbttag = getRootNBTTagCompound(nmsitem);
        if (rootnbttag == null) {
            rootnbttag = getNewNBTTag();
        }
        if (!valideCompound(item, comp).booleanValue()) {
            return item;
        }
        Object workingtag = gettoCompount(rootnbttag, comp);
        try {
            Method method = workingtag.getClass().getMethod("setBoolean", new Class[]{String.class, Boolean.TYPE});
            method.invoke(workingtag, new Object[]{key, d});
            nmsitem = setNBTTag(rootnbttag, nmsitem);
            return getBukkitItemStack(nmsitem);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return item;
    }

    public static Boolean getBoolean(ItemStack item, NBTCompound comp, String key) {
        Object nmsitem = getNMSItemStack(item);
        if (nmsitem == null) {
            System.out.println("Got null! (Outdated Plugin?)");
            return null;
        }
        Object rootnbttag = getRootNBTTagCompound(nmsitem);
        if (rootnbttag == null) {
            rootnbttag = getNewNBTTag();
        }
        if (!valideCompound(item, comp).booleanValue()) {
            return null;
        }
        Object workingtag = gettoCompount(rootnbttag, comp);
        try {
            Method method = workingtag.getClass().getMethod("getBoolean", new Class[]{String.class});
            return (Boolean) method.invoke(workingtag, new Object[]{key});
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static ItemStack remove(ItemStack item, NBTCompound comp, String key) {
        Object nmsitem = getNMSItemStack(item);
        if (nmsitem == null) {
            System.out.println("Got null! (Outdated Plugin?)");
            return null;
        }
        Object rootnbttag = getRootNBTTagCompound(nmsitem);
        if (rootnbttag == null) {
            rootnbttag = getNewNBTTag();
        }
        if (!valideCompound(item, comp).booleanValue()) {
            return item;
        }
        Object workingtag = gettoCompount(rootnbttag, comp);
        try {
            Method method = workingtag.getClass().getMethod("remove", new Class[]{String.class});
            method.invoke(workingtag, new Object[]{key});
            nmsitem = setNBTTag(rootnbttag, nmsitem);
            return getBukkitItemStack(nmsitem);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return item;
    }

    public static Boolean hasKey(ItemStack item, NBTCompound comp, String key) {
        Object nmsitem = getNMSItemStack(item);
        if (nmsitem == null) {
            System.out.println("Got null! (Outdated Plugin?)");
            return null;
        }
        Object rootnbttag = getRootNBTTagCompound(nmsitem);
        if (rootnbttag == null) {
            rootnbttag = getNewNBTTag();
        }
        if (!valideCompound(item, comp).booleanValue()) {
            return null;
        }
        Object workingtag = gettoCompount(rootnbttag, comp);
        try {
            Method method = workingtag.getClass().getMethod("hasKey", new Class[]{String.class});
            return (Boolean) method.invoke(workingtag, new Object[]{key});
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Set<String> getKeys(ItemStack item, NBTCompound comp) {
        Object nmsitem = getNMSItemStack(item);
        if (nmsitem == null) {
            System.out.println("Got null! (Outdated Plugin?)");
            return null;
        }
        Object rootnbttag = getRootNBTTagCompound(nmsitem);
        if (rootnbttag == null) {
            rootnbttag = getNewNBTTag();
        }
        if (!valideCompound(item, comp).booleanValue()) {
            return null;
        }
        Object workingtag = gettoCompount(rootnbttag, comp);
        try {
            Method method = workingtag.getClass().getMethod("c", new Class[0]);
            return (Set) method.invoke(workingtag, new Object[0]);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
