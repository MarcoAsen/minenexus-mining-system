package net.minenexus.newminesystem.nbt;

import org.bukkit.inventory.ItemStack;

public class NBT extends NBTCompound {
    private ItemStack bukkitItem;

    public NBT(ItemStack item) {
        super(null, null);
        this.bukkitItem = item.clone();
    }

    public ItemStack getItem() {
        return this.bukkitItem;
    }

    protected void setItem(ItemStack item) {
        this.bukkitItem = item;
    }
}
