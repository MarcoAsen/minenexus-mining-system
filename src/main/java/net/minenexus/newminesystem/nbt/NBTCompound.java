package net.minenexus.newminesystem.nbt;

import org.bukkit.inventory.ItemStack;

import java.util.Set;

public class NBTCompound {

    private String compundname;
    private NBTCompound parent;

    protected NBTCompound(NBTCompound owner, String name) {
        this.compundname = name;
        this.parent = owner;
    }

    public String getName() {
        return this.compundname;
    }

    public ItemStack getItem() {
        return this.parent.getItem();
    }

    protected void setItem(ItemStack item) {
        this.parent.setItem(item);
    }

    public NBTCompound getParent() {
        return this.parent;
    }

    public void setString(String key, String value) {
        setItem(NBTReflection.setString(getItem(), this, key, value));
    }

    public String getString(String key) {
        return NBTReflection.getString(getItem(), this, key);
    }

    public void setInteger(String key, int value) {
        setItem(NBTReflection.setInt(getItem(), this, key, Integer.valueOf(value)));
    }

    public Integer getInteger(String key) {
        return NBTReflection.getInt(getItem(), this, key);
    }

    public void setDouble(String key, double value) {
        setItem(NBTReflection.setDouble(getItem(), this, key, Double.valueOf(value)));
    }

    public double getDouble(String key) {
        return NBTReflection.getDouble(getItem(), this, key).doubleValue();
    }

    public void setBoolean(String key, boolean value) {
        setItem(NBTReflection.setBoolean(getItem(), this, key, Boolean.valueOf(value)));
    }

    public boolean getBoolean(String key) {
        return NBTReflection.getBoolean(getItem(), this, key).booleanValue();
    }

    public boolean hasKey(String key) {
        return NBTReflection.hasKey(getItem(), this, key).booleanValue();
    }

    public void removeKey(String key) {
        setItem(NBTReflection.remove(getItem(), this, key));
    }

    public Set<String> getKeys() {
        return NBTReflection.getKeys(getItem(), this);
    }

    public NBTCompound addCompound(String name) {
        setItem(NBTReflection.addNBTTagCompound(getItem(), this, name));
        return getCompound(name);
    }

    public NBTCompound getCompound(String name) {
        NBTCompound next = new NBTCompound(this, name);
        if (NBTReflection.valideCompound(getItem(), next).booleanValue()) {
            return next;
        }
        return null;
    }

}
